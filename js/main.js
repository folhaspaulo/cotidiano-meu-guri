var 
	allGal = [],
	loadedGallery= [],
	touches = [];

var isMobile = function(){
	if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
		return true;
	}
	else {
		return false;
	}
}

function cleanUp(value){
	return value.replace(/--/g, '\u2013')
				.replace(/\b'\b/g, "\u2019")
				.replace(/'\b/g, "\u2018")
				.replace(/'é/g,"\u2018é")
				.replace(/'É/g,"\u2018É")
				.replace(/'/g, "\u2019")
				.replace(/"\b/g, "\u201c")
				.replace(/"á/g,"\u201cá")
				.replace(/"é/g,"\u201cé")
				.replace(/"í/g,"\u201cí")
				.replace(/"ó/g,"\u201có")
				.replace(/"ú/g,"\u201cú")
				.replace(/"Á/g,"\u201cÁ")
				.replace(/"É/g,"\u201cÉ")
				.replace(/"Í/g,"\u201cÍ")
				.replace(/"Ó/g,"\u201cÓ")
				.replace(/"Ú/g,"\u201cÚ")
				.replace(/"\[/g,"\u201c[")
				.replace(/"/g, "\u201d")
				.replace(/\b",/g, "\u201d,")
				.replace(/m3/g,'m³')
				.replace(/m2/g,'m²')
				.replace(/O2/g,'O₂')
				.replace(/\u002E\u002E\u002E/g,'…')
				.replace(/&#8747;/g,'"');
}

function corrige(txt) {

	var result = ''

	if(txt.search('<')>=0 && txt.search('>')>=0){
		var text  = txt.split(/[<>]/);
		$.each(text, function(i, val){

			if(i==0){
				result+=cleanUp(val);
			} else if(i!=text.length-1){
				if(isEven(i)){
					result+='>'+cleanUp(val);
				} else {
					result+='<'+val;
				}

			} else {
				result+='>'+cleanUp(val);
			}

		});
	} else {
			result = cleanUp(txt);
	}

	return result;
}

function slug(nome) {
	var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
		to = 'aaaaaeeeeeiiiiooooouuuunc';

	nome = nome.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
	for (var i = 0, l = from.length ; i < l ; i++) {
		nome = nome.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	return nome;
}

function imgLoaded($this){
		
		$this.removeClass('lazy');
}



/* INICIO - Carrega Conteúdo*/
	function loadData(data){



		var content = document.getElementById('content'),
			abre = document.getElementById('section0'),
			slides = document.getElementById('slides');
			console.log(data.titulo_especial)




		if (isMobile()) {

$("#myVideo").css({"display":"none"})

		}else{
			var vid = document.createElement("video");
			vid.id="myVideo";
			vid.setAttribute("loop","");
			vid.setAttribute("muted","");
			vid.muted="true"
		

		}
			

		$.each(data.noticias, function(i, item) {
			var section = document.createElement("div"),
				cont = document.createElement("div"),
				textCont = document.createElement("div"),
				legenda = document.createElement("div");


			textCont.classList.add("textCont");
			section.classList.add("slide");
			legenda.classList.add("legenda");

			section.id="section"+(i+1);
			slides.appendChild(section);
			legenda.id="legenda"+i

			if (data.noticias[i].imagem_topo != undefined && data.noticias[i].imagem_topo != "") {
				textCont.style.backgroundImage="url("+data.noticias[i].imagem_topo+")";

			}



			if (data.noticias[i].titulo != undefined && data.noticias[i].titulo != "") {
				var nome = document.createElement("h2");
				nome.innerHTML=data.noticias[i].titulo;
				legenda.appendChild(nome);


			}

			if (data.noticias[i].texto != undefined && data.noticias[i].texto != "") {
				var texto = document.createElement("p");
				texto.innerHTML=data.noticias[i].texto;

				legenda.appendChild(texto);

			}
			if (data.noticias[i].chapeu != undefined && data.noticias[i].chapeu != "") {
				var audio = document.createElement("audio"),
					audCont= document.createElement("div"),
					audContImg= document.createElement("img"),
					audContP= document.createTextNode("Clique para ouvir o audio");

				
					audContImg.setAttribute("src","images/audio-01.svg");
					audCont.appendChild(audContImg);

					audCont.appendChild(audContP);
					audCont.classList.add("aud");


				audio.innerHTML=' <source src="'+data.noticias[i].chapeu+'" type="audio/mp3">'
				audio.id="audio"+i;

				textCont.appendChild(audCont);
				textCont.appendChild(audio);

				audCont.addEventListener("click",function(){

						audio.play();
			
					
				}) 

			}
			if (data.noticias[i].origem != undefined && data.noticias[i].origem != "") {
				if (isMobile()== false) {
				var video = document.createElement("video");
				video.innerHTML=' <source src="'+data.noticias[i].origem+'" type="video/mp4">'
				video.id="video"+i;
				video.setAttribute("autoplay","");
				video.setAttribute("loop","")
				video.setAttribute("muted","true");
				video.muted="true"
				textCont.appendChild(video);
					}
			}

			if (data.noticias[i].date != undefined && data.noticias[i].date != "") {
			var imgM= document.createElement("FIGURE");
				imgM.innerHTML=' <img src="'+data.noticias[i].date+'">'
				imgM.id="img"+i;

				textCont.appendChild(imgM);
			}

			if (data.noticias[i].id != undefined && data.noticias[i].id != "1" && data.noticias[i].id != " ") {
			var expe= document.createElement("P");
				expe.innerHTML=data.noticias[i].id
				expe.classList.add("expe")

				section.appendChild(expe);
			}

			textCont.appendChild(legenda);
			section.appendChild(textCont);






		})




	}
/* FINAL - Carrega Conteúdo*/





(function(){
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonpCallback: 'sabatina01',
		contentType: 'text/plain',
		success: function(data) {
			loadData(data);
						$('#content').fullpage({
				afterRender: function(){
					if (isMobile()==false) {
				
				}
				}
			});

		}
	});
	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			$('.loading').fadeOut(150);
			
			clearInterval(interval);
		}
	}, 500);

})();